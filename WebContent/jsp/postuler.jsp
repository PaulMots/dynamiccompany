<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<s:head/>
<sj:head/>
<sb:head/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">My company</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="#">Home</a></li>
              <li><a href="#">About</a></li>
              <li class="active"><a href="/dynamiccompany/jsp/postuler.jsp">Nous rejoindre</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="../navbar-fixed-top/">Admin</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main component for a primary marketing message or call to action -->
      
      <!-- Jumbotron -->
      <div class="jumbotron">
        <s:form action="save" enctype="multipart/form-data" theme="bootstrap" cssClass="form-horizontal"
                    label="Candidature spontanée">
                <s:textfield
                        label="Prénom"
                        name="firstname"
                        cssClass="input-sm"
                        elementCssClass="col-sm-3"
                        tooltip="Saisir votre prénom"/>

                <s:textfield
                        label="Nom de famille"
                        name="lastName"
                        cssClass="input-sm"
                        elementCssClass="col-sm-3"
                        tooltip="Saisir votre nom de famile"/>

                <s:textarea
                        tooltip="Merci d'indiquer vos motivations"
                        label="Motivations"
                        name="motivation"
                        cols="20"
                        rows="3"/>

                <s:select
                        tooltip="Merci d'indiquer le type de contrat recherché"
                        label="Type de contrat recherché"
                        elementCssClass="col-sm-4"
                        list="{'CDI', 'CDD', 'Stage'}"
                        name="contract"
                        emptyOption="false"/>

                <s:checkboxlist
                        tooltip="Choisir les lieux qui vous intéressent"
                        label="Nos implantations"
                        list="{'Paris', 'Rennes', 'Nantes', 'Nice'}"
                        name="towns"/>

                <s:radio
                        tooltip="Indiquer le nombre d'années d'expérience"
                        label="Nombre d'années d'expérience"
                        list="{'0-3', '3-10', '10+'}"
                        name="experience"
                        cssErrorClass="foo"/>

                <s:file
                        tooltip="Télécharger un CV"
                        label="CV"
                        name="cv"/>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-9">
                        <s:submit cssClass="btn btn-primary"/>
                    </div>
                </div>
            </s:form>
      </div>

    </div> <!-- /container -->
</body>
</html>	